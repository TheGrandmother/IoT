Identities
========================

To interoperate on the network, an entity, be it a thing, service or application, identifies itself using one or more identities. The following types of
identities are available:

* **Network identity**. The underlying XMPP protocol, used in IoT Harmnization, provides all connected entities with a global network identity. Each broker
controls its own domain, and set of accounts. The combination of account and domain, written in the form `account@domain`, is called a **bare JID**,
and is in IoT Harmonization often referrred to as the **network identity** of the entity. (JID = Jabber ID, Jabber being the name of the original project 
where XMPP was developed.) Once connected, a random resource is assigned to the connection. The **full JID**, written `account@domain/resource` is the 
network address of the entity. There may be multplie connections open using the same account at the same time. There may therefore be multiple
full JIDs available, for one bare JID.

	The XMPP address format is defined in [RFC 7622](https://tools.ietf.org/html/rfc7622). The XMPP protocol is defined in
	[RFC 6120](https://tools.ietf.org/html/rfc6120) and [RFC 6121](https://tools.ietf.org/html/rfc6121).

* **Conceptual identity**. The conceptual identity, such as serial number, manufacturer, make, model, etc., are used to disciver devices and pair devices
with their corresponding owners. More information about this, is available in the [discovery section](Discovery.md). Discovery is the process of matching
the conceptual identity with the corresponding network identity, and allow each device to know who is its corresponding owner.

* **User identity**. IoT Harmonization requests can be tagged with the identity of the user originating a request. This is done using X.509 certificates
	and [tokens](Tokens.md). It allows owners to make security decisions based on the identity of the calling user, instead of the network identities
	of the entities making requests.

* **Service identity**. IoT Harmonization requests can also be tagged with the identity of the service originating a request. As with user identities,
	it is done using X.509 certificates and [tokens](Tokens.md), and allows owners to make security decisions based on the identity of the calling service.

* **Device idetity**. Requests can also be tagged with the identity of the user originating device. It is also done using X.509 certificates
	and [tokens](Tokens.md). Security decisions can be based on the identity of the calling device, machine or host.
