Legal Identities
=====================

It is possible to assign a legal identity to an account. By assigning a legal identity to the account, it becomes possible for the account to sign legal
contracts. Such contracts can be used by owners to regulate conditions for accessing their things, allowing for automation of decision support and provisioning.

| Legal Identities                                                      ||
| ------------|----------------------------------------------------------|
| Namespace:  | urn:ieee:iot:leg:id:1.0                                  |
| Schema:     | [LegalIdentities.xsd](Schemas/LegalIdentities.xsd)       |
